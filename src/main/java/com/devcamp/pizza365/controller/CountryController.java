package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;
	@CrossOrigin
	@PostMapping("/country/create")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry country = new CCountry();
			country.setCountryName(cCountry.getCountryName());
			country.setCountryCode(cCountry.getCountryCode());
			country.setRegions(cCountry.getRegions());
			CCountry savedRole = countryRepository.save(country);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Country: "+e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@PutMapping("/country/update/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			newCountry.setRegions(cCountry.getRegions());
			CCountry savedCountry = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	/**
	 * Viết method delete a country: deleteCountryById() sử dụng hàm deleteById() của CountryRepository 
	 * @param id
	 * @return
	 */
	@CrossOrigin
	@DeleteMapping("/country/delete/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			Optional<CCountry> optional= countryRepository.findById(id);
			if (optional.isPresent()) {
				countryRepository.deleteById(id);
			}else {
				//countryRepository.deleteById(id);
			}			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/details/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@GetMapping("/country/all")
	public List<CCountry> getAllCountry() {
		return countryRepository.findAll();
	}
	@CrossOrigin
	@GetMapping("/country/code/{code}")
	public CCountry getCountryByCode(@PathVariable String code) {
		return countryRepository.findByCountryCode(code);
	}
	/**
	 * Viết method kiểm tra country có trong CSDL hay không: checkCountryById() sử dụng hàm existsById() của CountryRepository 
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/country-count")
	public long countCountry() {
		return countryRepository.count();
	}
	/**
	 * Viết method kiểm tra country có trong CSDL hay không: checkCountryById() sử dụng hàm existsById() của CountryRepository
	 * @param id
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/country/check/{id}")
	public boolean checkCountryById(@PathVariable Long id) {
		return countryRepository.existsById(id);
	}
	/**
	 * Tìm country có chứa giá trị trong country code 
	 * @param code
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/country/containing-code/{code}")
	public List<CCountry> getCountryByContainingCode(@PathVariable String code) {
		return countryRepository.findByCountryCodeContaining(code);
	}
}
